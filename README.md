# XKCD
Originally started as a BYU CS260/CS201R project.

It works, all bugs that have been found have been resolved.

It requires both a separate web server, such as apache, to deliver the page and it must have the node.js proxy as a proxy server on the web server that delivered the original page.

A working example is found at https://bryceml.us/xkcd/

Version 1.1 adds a link to get to the original xkcd page if desired.

Version 1.2 takes into account that the api now sends https urls for the image.

Last used with nodejs version 10

# Apache Config to proxy for nodejs
```
ProxyRequests Off
ProxyPass /node http://localhost:12894/
ProxyPassReverse /node http://localhost:12894/
```

# Systemd Service File /etc/systemd/system/xkcd.service
```
[Service]
ExecStart=/usr/bin/nodejs /var/www/html/xkcd/xkcd-nodejs-proxy.js
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=nodejs-xkcd
User=www-data
Group=www-data

[Install]
WantedBy=network-online.target
```
